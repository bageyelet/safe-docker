FROM tensorflow/tensorflow:1.14.0-py3

RUN apt-get update && \
    apt-get upgrade -y

RUN apt-get install -y \
        wget           \
        curl           \
        sudo           \
        git

RUN groupadd --gid 1000 safe \
    && useradd --uid 1000 --gid safe --shell /bin/bash --create-home safe
RUN echo "safe ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/safe && \
    chmod 0440 /etc/sudoers.d/safe

USER safe
WORKDIR /home/safe

RUN git clone https://github.com/gadiluna/SAFE /home/safe/SAFE
RUN cd /home/safe/SAFE && git checkout c9be3f47887d4107c7c9d5822ac3f6aaa9e0c31f

# Small bugfix
RUN sed -i '42 s/$/ if "scale" in op else "0"/' /home/safe/SAFE/asm_embedding/FunctionAnalyzerRadare.py

RUN pip install --user -r /home/safe/SAFE/requirements.txt
RUN cd /home/safe/SAFE && ./download_model.sh 2> /dev/null

RUN wget https://github.com/radareorg/radare2/archive/5.1.1.tar.gz -P /home/safe
RUN cd /home/safe && tar xf 5.1.1.tar.gz && rm 5.1.1.tar.gz
RUN cd /home/safe/radare2-5.1.1 && sudo ./sys/install.sh

COPY --chown=safe:safe ./find_fun.py /home/safe/find_fun.py
COPY --chown=safe:safe ./tests       /home/safe/tests
