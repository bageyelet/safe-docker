import r2pipe
import sys

sys.path.insert(0, "/home/safe/SAFE")
import safe as safe_module
from collections import namedtuple
from sklearn.metrics.pairwise import cosine_similarity


FunctionInfo = namedtuple("FunctionInfo", ["address", "nbbs"])

MODEL_PATH = "/home/safe/SAFE/data/safe.pb"
CONVERTER_JSON_PATH = "/home/safe/SAFE/data/i2v/word2id.json"


def usage():
    sys.stderr.write("python3 %s <bin_with_symbols> <bin_without_symbols> <addr>\n" % sys.argv[0])
    exit(1)

def to_int(v):
    if v[:2] == "0x":
        return int(v, 16)
    return int(v)

def is_suitable_function(fun, target_fun):
    return fun.nbbs * 0.70 <= target_fun.nbbs <= fun.nbbs * 1.30

def get_info_functions(binary):
    r2 = r2pipe.open(binary)
    r2.cmd("aaa")

    functions = list()
    for fun_raw in r2.cmdj("aflj"):
        functions.append(FunctionInfo(
            address=fun_raw["offset"],
            nbbs=fun_raw["nbbs"]))

    r2.quit()
    return functions

def get_info_function(binary, addr):
    r2 = r2pipe.open(binary)
    r2.cmd("aaa")

    fun_info_raw = r2.cmdj("afij@%#x" % addr)
    if fun_info_raw is None:
        return None

    assert len(fun_info_raw) == 1
    res = FunctionInfo(
        address=addr,
        nbbs=fun_info_raw[0]["nbbs"])

    r2.quit()
    return res


class SAFE(object):
    def __init__(self):
        self.converter  = safe_module.InstructionsConverter(CONVERTER_JSON_PATH)
        self.normalizer = safe_module.FunctionNormalizer(max_instruction=150)
        self.embedder   = safe_module.SAFEEmbedder(MODEL_PATH)
        self.embedder.loadmodel()
        self.embedder.get_tensor()

        self.cache = dict()

    def embedd_function(self, filename, address):
        if filename not in self.cache:
            analyzer = safe_module.RadareFunctionAnalyzer(filename, use_symbol=False, depth=0)
            functions = analyzer.analyze()
            self.cache[filename] = functions

        functions = self.cache[filename]
        instructions_list = None
        for function in functions:
            if functions[function]['address'] == address:
                instructions_list = functions[function]['filtered_instructions']
                break
        if instructions_list is None:
            sys.stderr.write("Function @ %#x not found\n" % address)
            return None

        converted_instructions = self.converter.convert_to_ids(instructions_list)
        instructions, length = self.normalizer.normalize_functions([converted_instructions])
        embedding = self.embedder.embedd(instructions, length)
        return embedding


if __name__ == "__main__":
    if len(sys.argv) < 4:
        usage()

    bin_symb   = sys.argv[1]
    bin_nosymb = sys.argv[2]
    addr_symb  = to_int(sys.argv[3])

    target_fun = get_info_function(bin_symb, addr_symb)
    if target_fun is None:
        sys.stderr.write("%#x is not a valid function address\n" % addr_symb)
        exit(1)

    sys.stderr.write("[+] Target function @ %#x with %d bbs\n" % (target_fun.address, target_fun.nbbs))

    safe = SAFE()
    target_emb = safe.embedd_function(bin_symb, addr_symb)
    if target_emb is None:
        exit(1)

    sys.stderr.write("[+] Calculated target embedding\n")

    suitable_functions  = list(filter(
        lambda f: is_suitable_function(f, target_fun),
        get_info_functions(bin_nosymb)))
    assert len(suitable_functions) > 0  # No suitable function

    sys.stderr.write("[+] Found %d suitable functions\n" % len(suitable_functions))

    suitable_embeddings = list(map(
        lambda f: safe.embedd_function(bin_nosymb, f.address),
        suitable_functions))

    suitable_similarity = list(map(
        lambda emb: cosine_similarity(target_emb, emb),
        suitable_embeddings))

    found_score, found_idx = max(zip(suitable_similarity, range(len(suitable_similarity))), key=lambda x: x[0])
    print("The function with the highest similarity score (%s) is @ %#x" % \
        (found_score[0][0], suitable_functions[found_idx].address))
